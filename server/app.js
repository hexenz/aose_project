const Express = require("express");
const BodyParser = require("body-parser");
const path = require("path");
const passport = require('passport');
const Mongoose = require('mongoose');
const session = require('express-session');
const app = new Express();
require('./db')(app);
require('./config/passport')(passport);


// module.exports = {
//     Product: require('./src/product/product.model')
// };

const PORT = 4000;

app.use(BodyParser.json());
app.use(BodyParser.urlencoded());

app.get("/*", Express.static(path.join(__dirname, "client")));

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.listen(PORT, () => {
    console.log("Listening on port 4000");
});



