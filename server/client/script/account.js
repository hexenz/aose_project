$(() => {
    const accountPanel = $("#account-panel");
    const loginBtn = $("#login-register");

    let cartList = JSON.parse(localStorage.getItem('cart'));
    console.log(cartList);


    const getCurrentUser = () => {
        fetch('/profile', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            if (data.isEmpty() == false && data.username) {
                loginBtn.detach();
                accountPanel.append('<p>Hello, ' + data.username + '! <a href="/cart"><i class="fas fa-shopping-cart"></i></a><a href="/logout" id="logout"><i class="fas fa-sign-out-alt"></i></a></p>');
            } else {
                accountPanel.append('<a href="/login" id="login-register" class="login">Login/Register</a>');
            }
        })
    };

    getCurrentUser();

    Object.prototype.isEmpty = () => {
        for (var key in this) {
            if (this.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    let logoutBtn = $("#logout");
    logoutBtn.click(() => {
        localStorage.clear();
        location.reload();
    })

});