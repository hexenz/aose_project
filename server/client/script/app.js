$(() => {
    // Basic declarations for easier ID manipulation
    const display = $("#display");
    const getAll = $("#getAll");
    const getVegetables = $("#getVegetables");
    const getFruits = $("#getFruits");
    const cart = [];

    // GET request for products
    const getProducts = () => {
        fetch('/api/products', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);
            displayProducts(data);
        });
    }

    // Getting the products 
    getProducts();

    // GET request for filtering the products
    const fruits = () => {
        fetch('/api/products/fruits', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);
            displayProducts(data);
        });
    }

    // GET request for filtering the products
    const vegetables = () => {
        fetch('/api/products/vegetables', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);
            displayProducts(data);
        });
    }

    // Displaying the products
    const displayProducts = (data) => {
        data.forEach((product) => {
            let ids = buildIDS(product);
            display.append(buildTemplate(product, ids));
            addToCart(product, ids.quantity, ids.addID)
        })
    }

    // Helper function that generates ids for the products
    const buildIDS = (product) => {
        return {
            listItemID: "listItem_" + product._id,
            productID: "product_" + product._id,
            addID: "add_" + product._id,
            quantity: "quantity_" + product._id
        }
    }

    // Adding to cart functionality
    const addToCart = (product, quantity, addID) => {
        let addBtn = $(`#${addID}`);
        console.log(quantity);
        addBtn.click(() => {
            let q = parseInt($(`#${quantity}`).val());
            product.qty = q;            
            containsObject(product, cart, q);            
            localStorage.setItem("cart", JSON.stringify(cart));
            console.log(cart);
        })
    }

    // Helper function that checks if the product is already in the shopping cart
    const containsObject = (obj, list, q)  => {
        let i;
        for (i = 0; i < list.length; i++) {
            if (list[i] === obj) {
                return obj.qty = q
            }
        }       
        return list.push(obj);
    }

    // Helper function used to add new items to the unordered list
    const buildTemplate = (product, ids) => {
        return `<li class="list-group-item w3-quarter w3-container" id="${ids.listItemID}">
                  <div class="grid-item">
                    <div class="product-image">
                        <img src="${product.img}">
                    </div>
                    <h5 class="title">${product.name}</h5>
                    <div class="price">Price: &#36;${product.price}</div>
                    <div class="add">
                        <input class="val" type="number" min="1" max="20" value="1" id=${ids.quantity}>
                        <input class="btn1" type="submit" value="ADD TO CART" id=${ids.addID}>
                    </div>
                    </div>
                </li>`;
    }

    // Event listener on button that gets the products
    getAll.click(() => {
        display.empty();
        getProducts();
    })

    // Event listener on button that gets the fruits
    getFruits.click(() => {
        display.empty();
        fruits();
    })

    // Event listener on button that gets the vegetables
    getVegetables.click(() => {
        display.empty();
        vegetables();
    })
});