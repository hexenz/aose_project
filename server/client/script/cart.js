$(() => {
    let total = 0;
    const display = $("#display");
    const subTotal = $("#total");
    const user = [];
    let userID;


    const getCurrentUser = () => {
        fetch('/profile', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            userID = data._id;
        })
    }

    getCurrentUser();

    let cartList = JSON.parse(localStorage.getItem('cart'));
    console.log(cartList);

    // Helper function that generates ids for the products
    const buildIDS = (product) => {
        return {
            listItemID: "listItem_" + product._id,
            productID: "product_" + product._id,
            deleteID: "delete_" + product._id,
        }
    }

    const deleteProduct = (product, listItemID, deleteID) => {
        let deleteBtn = $(`#${deleteID}`);
        deleteBtn.click(() => {
            $(`#${listItemID}`).remove();
            let index = cartList.indexOf(product);
            cartList.splice(index, 1);
            localStorage.setItem('cart', JSON.stringify(cartList));
            location.reload();
        })
    }

    // Helper function used to add new items to the unordered list
    const buildTemplate = (product, ids) => {
        return `<li class="cart-list" id="${ids.listItemID}">
                  <div class="item">
                    <p class="product-name">${product.name}</p>
                    <span>
                        <p class="qty">amount: ${product.qty}</p>
                        <input class="delete" type="submit" value="X" id=${ids.deleteID}>
                    </span>
                 </div>
                </li>`;
    }

    for (let i = 0; i < cartList.length; i++) {
        let ids = buildIDS(cartList[i]);
        total += cartList[i].qty * cartList[i].price;
        display.append(buildTemplate(cartList[i], ids));
        deleteProduct(cartList[i], ids.listItemID, ids.deleteID);
    };


    const showTotal = (total) => {
        if (cartList.length == 0) {
            display.append(`<div></div>`)
        } else {
            return `
                <div class="total">
                    <div>Total: ${total}</div>
                    <button class="order" type="button" id="send-order">ORDER</button>
                </div>`;
        }
    }

    display.append(showTotal(total));
    console.log(total);


    let order = $("#send-order");
    order.click(() => {
        fetch(`/order/${userID}`, {
            method: 'put',
            body: JSON.stringify({ credit: parseInt(total) }),
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        }).then((res) => {
            display.append(`<h3>Thank you for your purchase!</h3>`)
            return res.json();
        })
    })
})


