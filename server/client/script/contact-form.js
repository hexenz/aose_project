$(() => {const myform = $("form#myform");
myform.submit((event) => {
	event.preventDefault();

	let params = myform.serializeArray().reduce(function(obj, item) {
     obj[item.name] = item.value;
     return obj;
  }, {});

  // Change to your service ID, or keep using the default service
  const service_id = "greengarden_gmail";

  const template_id = "template_KjMWqti6";
  myform.find("button").text("Sending...");
  emailjs.send(service_id, template_id, params)
  	.then(() => { 
       alert("Sent!");
       myform.find("button").text("Send");
     }, (err) => {
       alert("Send email failed!\r\n Response:\n " + JSON.stringify(err));
       myform.find("button").text("Send");
    });
    
  return false;
});
$(".submit").click(() => {
    $("#myform")[0].reset();
    });
});