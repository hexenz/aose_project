$(() => {
    const display = $("#display");

    const getPatrons = () => {
        fetch('/halloffame', { method: "get" }).then((response) => {
            return response.json();
        }).then((data) => {
            console.log(data);
            displayPatrons(data);
        });
    }
    
    getPatrons();

    // Displaying the products
    const displayPatrons = (data) => {
        data.forEach((patron) => {
            let ids = buildIDS(patron);
            display.append(buildTemplate(patron, ids));
        })
    }

    const buildIDS = (patron) => {
        return {
            listItemID: "listItem_" + patron._id,
            patronID: "patron_" + patron._id
        }
    }

    // Helper function used to add new items to the unordered list
    const buildTemplate = (patron, ids) => {
        return `<li class="list-group-item" id="${ ids.listItemID }">
                    <div class="service-icon">
                         <img src="https://image.flaticon.com/icons/png/512/25/25267.png">
                    </div
                     </div>
                        <div class="user-title">${ patron.username }</div>
                        <div class="user-price">Amount: ${ patron.credit }</div>
                    </div>
                </li>`;
    }
});