const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// Load User model
const User = require('../src/user/user.model');

module.exports = function (passport) {
    passport.use(
        new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, (username, password, done) => {
            // Match user
            User.findOne({ username: username }).then(user => {
                if (!user) {
                    return done(null, false, { message: 'That username/password is not correct' });
                }
                // Match password
                bcrypt.compare(password, user.password, (err, isMatch) => {
                    console.log("im here");
                    if (err) throw err;
                    if (isMatch) {
                        console.log(user);
                        return done(null, user);
                    } else {
                        return done(null, false, { message: 'That username/password is not correct' });
                    }
                });
            }).catch(err => console.log(err));
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });
};