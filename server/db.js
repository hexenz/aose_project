function DB (app) {
	const Mongoose = require('mongoose');

	Mongoose.connect('mongodb://localhost/aoseproject', { useNewUrlParser: true }, (err) => {
		if (!err) {
			console.log('Connected to database');
			loadFiles(err);
		} else {
			console.log('Error in DB connection: ' + err)
		}
	});

	function loadFiles (error) {
		if (error) {
			console.log('Mongo error:', error);
		} else {
			const modules = ['user', 'product'];

			modules.forEach(function (module) {
				require(__dirname + `/src/${module}/${module}.router`)(app, Mongoose);
			});
			console.log('Mongoose loaded');
		}
	}
}

module.exports = DB;