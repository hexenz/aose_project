module.exports = function (Mongoose) {

    const Product = require('./product.model');

    this.getAll = (success, fail) => {
        Product.find((err, result) => {
            err ? fail(err) : success(result);
        });
    };

    this.getFruits = (success, fail) => {
        Product.find({ category: "fruit" }, (err, result) => {
            err ? fail(err) : success(result); 
        })
    };
    
    this.getVegetables = (success, fail) => {
        Product.find({ category: "vegetable" }, (err, result) => {
            err ? fail(err) : success(result); 
        })
    };

}