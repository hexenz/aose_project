const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const productSchema = new Schema({
	name: String,
	price: Number,
	img: String,
	category: String,
});

// productSchema.set('toJSON', { virtuals: true });
Mongoose.model("Product", productSchema);
const Product = Mongoose.models.Product;

module.exports = Product;
