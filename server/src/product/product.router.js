module.exports = (app, Mongoose) => {
    const ProductManager = require('./product.manager');
    const Product = require('./product.model');
    let prdManager = new ProductManager(Mongoose);

    app.get("/api/products", (req, res) => {
        prdManager.getAll((error, result) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(200).json(result);
            }
        });
    });

    app.get("/api/products/fruits", (req, res) => {
        prdManager.getFruits((error, result) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(200).json(result);
            }
        });
    });

    app.get("/api/products/vegetables", (req, res) => {
        prdManager.getVegetables((error, result) => {
            if (error) {
                res.status(500).json(error);
            } else {
                res.status(200).json(result);
            }
        });
    });

    app.post("/addproduct", (req, res) => {
        const todo = req.body;
        Product.create(todo, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                res.json(result);
            }
            console.log("Added " + req.body);
        })
    });
}
