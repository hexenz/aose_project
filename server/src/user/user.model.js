const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const userSchema = Mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true
    },
    credit: {
        type: Number,
        default: 0
    }
});

Mongoose.model("User", userSchema);
const User = Mongoose.models.User;

module.exports = User;