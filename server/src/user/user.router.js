module.exports = (app, Mongoose) => {
    const bcrypt = require('bcrypt');
    const passport = require('passport');
    const UserManager = require('./user.manager');
    const User = require('./user.model');
    let userManager = new UserManager(Mongoose);
    const { ensureAuthenticated } = require('../../config/auth');

    // Homepage route    
    app.get('/', (req, res) => {
        res.sendFile('index.html', { root: './client' });
    });

    // Products route
    app.get('/products', (req, res) => {
        res.sendFile('products.html', { root: './client' })
    });

    // About route
    app.get('/about', (req, res) => {
        res.sendFile('about.html', { root: './client' })
    });

    // Register route
    app.get('/register', (req, res) => {
        res.sendFile('register.html', { root: './client' })
    });

    // Hall of fame route
    app.get('/hof', (req, res) => {
        res.sendFile('trees.html', { root: './client' })
    });

    // Contact route
    app.get('/contact', (req, res) => {
        res.sendFile('contact.html', { root: './client' })
    });

    // Login route
    app.get('/login', (req, res) => {
        res.sendFile('login.html', { root: './client' })
    });

    app.get('/cart', (req, res) => {
        res.sendFile('cart.html', { root: './client' })
    })

    // User registration
    app.post("/user/register", (req, res) => {
        const { username, password, email } = req.body;
        User.findOne({ username: username })
            .then(user => {
                if (user) {
                    console.log("Username already exists");
                } else {
                    console.log(req.body);
                    const newUser = new User({
                        username,
                        password,
                        email
                    });

                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(newUser.password, salt, (err, hash) => {
                            if (err) throw err;
                            newUser.password = hash;
                            User.create(newUser, (err, result) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    res.redirect('/login');
                                }
                            });
                        });
                    })
                };
            })
    });


    // User Authentication
    app.post('/user/login', (req, res, next) => {
        passport.authenticate('local', {
            successRedirect: '/products',
            failureRedirect: '/login',
        })(req, res, next);
    });

    // GET the current logged user
    app.get('/profile', (req, res, next) => {
        res.send(req.user || {});
    })

    // Logout route
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    // GET patreons
    app.get('/halloffame', (request, response) => {
        User.find({ credit: { $gt: 200 } }, (error, result) => {
            if (error) {
                response.status(500).json(error);
            } else {
                response.status(200).json(result);
            }
        }
        );
    });

    // Updating the credit after making a purchase
       app.put("/order/:id", (req, res) => {
        const userID = req.params.id;
        User.findByIdAndUpdate({ _id: userID }, { $inc: req.body }, { returnOriginal: false }, (err, result) => {
            if (err) {
                console.log(err);
            } else {
                res.json(result);
            }
        })
    });
}